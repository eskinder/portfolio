[![CI](https://github.com/eskinderg/eskinderg.github.io/actions/workflows/CI.yml/badge.svg)](https://github.com/eskinderg/eskinderg.github.io/actions/workflows/CI.yml)
## How to Run locally
<p>Run</p>

```
git clone https://github.com/eskinderg/portfolio.git
```

```
cd portfolio
```

```
npm install
```

```
npm run start
```
<p>Then Navigate to <a href="http://localhost:4200/" target="_blank">http://localhost:4200/</a></p>
